/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */     
#include "queue.h"

#include "../Components/esp32_lib/Inc/debug_io.h"
#include "../Components/esp32_lib/Inc/esp32.h"
#include "../Components/esp32_lib/Inc/esp32_threads.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .priority = (osPriority_t) osPriorityBelowNormal7,
  .stack_size = 128 * 4
};
/* Definitions for esp_coord_task */
osThreadId_t esp_coord_taskHandle;
const osThreadAttr_t esp_coord_task_attributes = {
  .name = "esp_coord_task",
  .priority = (osPriority_t) osPriorityNormal1,
  .stack_size = 256 * 4
};
/* Definitions for esp_rx_task */
osThreadId_t esp_rx_taskHandle;
const osThreadAttr_t esp_rx_task_attributes = {
  .name = "esp_rx_task",
  .priority = (osPriority_t) osPriorityAboveNormal,
  .stack_size = 256 * 4
};
/* Definitions for esp_ctrl_cons */
osThreadId_t esp_ctrl_consHandle;
const osThreadAttr_t esp_ctrl_cons_attributes = {
  .name = "esp_ctrl_cons",
  .priority = (osPriority_t) osPriorityNormal7,
  .stack_size = 512 * 4
};
/* Definitions for esp_ctrl_prod */
osThreadId_t esp_ctrl_prodHandle;
const osThreadAttr_t esp_ctrl_prod_attributes = {
  .name = "esp_ctrl_prod",
  .priority = (osPriority_t) osPriorityNormal6,
  .stack_size = 128 * 4
};
/* Definitions for debug_tx_task */
osThreadId_t debug_tx_taskHandle;
const osThreadAttr_t debug_tx_task_attributes = {
  .name = "debug_tx_task",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 256 * 4
};
/* Definitions for esp_transp */
osThreadId_t esp_transpHandle;
const osThreadAttr_t esp_transp_attributes = {
  .name = "esp_transp",
  .priority = (osPriority_t) osPriorityNormal3,
  .stack_size = 256 * 4
};
/* Definitions for transparent_Queue */
osMessageQueueId_t transparent_QueueHandle;
const osMessageQueueAttr_t transparent_Queue_attributes = {
  .name = "transparent_Queue"
};
/* Definitions for esp32_rx_control_Queue */
osMessageQueueId_t esp32_rx_control_QueueHandle;
const osMessageQueueAttr_t esp32_rx_control_Queue_attributes = {
  .name = "esp32_rx_control_Queue"
};
/* Definitions for esp32_rx_dma_queue_id */
osMessageQueueId_t esp32_rx_dma_queue_idHandle;
const osMessageQueueAttr_t esp32_rx_dma_queue_id_attributes = {
  .name = "esp32_rx_dma_queue_id"
};
/* Definitions for esp32_tx_control_Queue */
osMessageQueueId_t esp32_tx_control_QueueHandle;
const osMessageQueueAttr_t esp32_tx_control_Queue_attributes = {
  .name = "esp32_tx_control_Queue"
};
/* Definitions for debug_tx_dma_queue_id */
osMessageQueueId_t debug_tx_dma_queue_idHandle;
const osMessageQueueAttr_t debug_tx_dma_queue_id_attributes = {
  .name = "debug_tx_dma_queue_id"
};
/* Definitions for usr_rx_dma_queue_id */
osMessageQueueId_t usr_rx_dma_queue_idHandle;
const osMessageQueueAttr_t usr_rx_dma_queue_id_attributes = {
  .name = "usr_rx_dma_queue_id"
};
/* Definitions for ESP_MSG_Timeout01 */
osTimerId_t ESP_MSG_Timeout01Handle;
const osTimerAttr_t ESP_MSG_Timeout01_attributes = {
  .name = "ESP_MSG_Timeout01"
};
/* Definitions for ESP_MSG_Timeout02 */
osTimerId_t ESP_MSG_Timeout02Handle;
const osTimerAttr_t ESP_MSG_Timeout02_attributes = {
  .name = "ESP_MSG_Timeout02"
};
/* Definitions for ESP_MSG_Timeout03 */
osTimerId_t ESP_MSG_Timeout03Handle;
const osTimerAttr_t ESP_MSG_Timeout03_attributes = {
  .name = "ESP_MSG_Timeout03"
};
/* Definitions for sysMutex */
osMutexId_t sysMutexHandle;
const osMutexAttr_t sysMutex_attributes = {
  .name = "sysMutex"
};
/* Definitions for producer_sync_sem */
osSemaphoreId_t producer_sync_semHandle;
const osSemaphoreAttr_t producer_sync_sem_attributes = {
  .name = "producer_sync_sem"
};
/* Definitions for usr_msg_sync_sem */
osSemaphoreId_t usr_msg_sync_semHandle;
const osSemaphoreAttr_t usr_msg_sync_sem_attributes = {
  .name = "usr_msg_sync_sem"
};
/* Definitions for sys_sem */
osSemaphoreId_t sys_semHandle;
const osSemaphoreAttr_t sys_sem_attributes = {
  .name = "sys_sem"
};
/* Definitions for debug_msg_sync_sem */
osSemaphoreId_t debug_msg_sync_semHandle;
const osSemaphoreAttr_t debug_msg_sync_sem_attributes = {
  .name = "debug_msg_sync_sem"
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
   
/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);
void esp_coordinator_task(void *argument);
void esp_rx_dma_task(void *argument);
void esp_ctrl_consumer_task(void *argument);
void esp_ctrl_producting_task(void *argument);
void debug_tx_dma_task(void *argument);
void esp_transparent_data_task(void *argument);
void ESP_MSG_Timeout01_Callback(void *argument);
void ESP_MSG_Timeout02_Callback(void *argument);
void ESP_MSG_Timeout03_Callback(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{

}

__weak unsigned long getRunTimeCounterValue(void)
{
return 0;
}
/* USER CODE END 1 */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
       
  /* USER CODE END Init */
  /* Create the mutex(es) */
  /* creation of sysMutex */
  sysMutexHandle = osMutexNew(&sysMutex_attributes);

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* creation of producer_sync_sem */
  producer_sync_semHandle = osSemaphoreNew(1, 1, &producer_sync_sem_attributes);

  /* creation of usr_msg_sync_sem */
  usr_msg_sync_semHandle = osSemaphoreNew(1, 1, &usr_msg_sync_sem_attributes);

  /* creation of sys_sem */
  sys_semHandle = osSemaphoreNew(1, 1, &sys_sem_attributes);

  /* creation of debug_msg_sync_sem */
  debug_msg_sync_semHandle = osSemaphoreNew(1, 1, &debug_msg_sync_sem_attributes);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* creation of ESP_MSG_Timeout01 */
  ESP_MSG_Timeout01Handle = osTimerNew(ESP_MSG_Timeout01_Callback, osTimerOnce, NULL, &ESP_MSG_Timeout01_attributes);

  /* creation of ESP_MSG_Timeout02 */
  ESP_MSG_Timeout02Handle = osTimerNew(ESP_MSG_Timeout02_Callback, osTimerOnce, NULL, &ESP_MSG_Timeout02_attributes);

  /* creation of ESP_MSG_Timeout03 */
  ESP_MSG_Timeout03Handle = osTimerNew(ESP_MSG_Timeout03_Callback, osTimerOnce, NULL, &ESP_MSG_Timeout03_attributes);

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* creation of transparent_Queue */
  transparent_QueueHandle = osMessageQueueNew (16, sizeof(serv_clt_msg_t), &transparent_Queue_attributes);

  /* creation of esp32_rx_control_Queue */
  esp32_rx_control_QueueHandle = osMessageQueueNew (16, sizeof(control_msg_t), &esp32_rx_control_Queue_attributes);

  /* creation of esp32_rx_dma_queue_id */
  esp32_rx_dma_queue_idHandle = osMessageQueueNew (10, sizeof(uint8_t), &esp32_rx_dma_queue_id_attributes);

  /* creation of esp32_tx_control_Queue */
  esp32_tx_control_QueueHandle = osMessageQueueNew (16, sizeof(control_msg_t), &esp32_tx_control_Queue_attributes);

  /* creation of debug_tx_dma_queue_id */
  debug_tx_dma_queue_idHandle = osMessageQueueNew (10, sizeof(debug_msg_t), &debug_tx_dma_queue_id_attributes);

  /* creation of usr_rx_dma_queue_id */
  usr_rx_dma_queue_idHandle = osMessageQueueNew (16, sizeof(uint8_t), &usr_rx_dma_queue_id_attributes);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* creation of esp_coord_task */
  esp_coord_taskHandle = osThreadNew(esp_coordinator_task, NULL, &esp_coord_task_attributes);

  /* creation of esp_rx_task */
  esp_rx_taskHandle = osThreadNew(esp_rx_dma_task, NULL, &esp_rx_task_attributes);

  /* creation of esp_ctrl_cons */
  esp_ctrl_consHandle = osThreadNew(esp_ctrl_consumer_task, NULL, &esp_ctrl_cons_attributes);

  /* creation of esp_ctrl_prod */
  esp_ctrl_prodHandle = osThreadNew(esp_ctrl_producting_task, NULL, &esp_ctrl_prod_attributes);

  /* creation of debug_tx_task */
  debug_tx_taskHandle = osThreadNew(debug_tx_dma_task, NULL, &debug_tx_task_attributes);

  /* creation of esp_transp */
  esp_transpHandle = osThreadNew(esp_transparent_data_task, NULL, &esp_transp_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */

void StartDefaultTask(void *argument)
{
	serv_clt_msg_t msg = {"hello worldhello world\r\n" ,24 ,TCP_CLIENT_LINK_ID};
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
	  HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_0);
	  if(esp.tcp_conn == TCP_CONNECTED)
	  {
		 xQueueSend(transparent_QueueHandle, &msg, 0);
	  }
    osDelay(500);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_esp_coordinator_task */
/**
* @brief Function implementing the esp_coord_task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_esp_coordinator_task */
__weak void esp_coordinator_task(void *argument)
{
  /* USER CODE BEGIN esp_coordinator_task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(100);
  }
  /* USER CODE END esp_coordinator_task */
}

/* USER CODE BEGIN Header_esp_rx_dma_task */
/**
* @brief Function implementing the esp_rx_task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_esp_rx_dma_task */
__weak void esp_rx_dma_task(void *argument)
{
  /* USER CODE BEGIN esp_rx_dma_task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(100);
  }
  /* USER CODE END esp_rx_dma_task */
}

/* USER CODE BEGIN Header_esp_ctrl_consumer_task */
/**
* @brief Function implementing the esp_ctrl_cons thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_esp_ctrl_consumer_task */
__weak void esp_ctrl_consumer_task(void *argument)
{
  /* USER CODE BEGIN esp_ctrl_consumer_task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(100);
  }
  /* USER CODE END esp_ctrl_consumer_task */
}

/* USER CODE BEGIN Header_esp_ctrl_producting_task */
/**
* @brief Function implementing the esp_ctrl_prod thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_esp_ctrl_producting_task */
__weak void esp_ctrl_producting_task(void *argument)
{
  /* USER CODE BEGIN esp_ctrl_producting_task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(100);
  }
  /* USER CODE END esp_ctrl_producting_task */
}

/* USER CODE BEGIN Header_debug_tx_dma_task */
/**
* @brief Function implementing the debug_tx_task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_debug_tx_dma_task */
__weak void debug_tx_dma_task(void *argument)
{
  /* USER CODE BEGIN debug_tx_dma_task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(100);
  }
  /* USER CODE END debug_tx_dma_task */
}

/* USER CODE BEGIN Header_esp_transparent_data_task */
/**
* @brief Function implementing the esp_transp thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_esp_transparent_data_task */
__weak void esp_transparent_data_task(void *argument)
{
  /* USER CODE BEGIN esp_transparent_data_task */
  /* Infinite loop */
  for(;;)
  {
    osDelay(100);
  }
  /* USER CODE END esp_transparent_data_task */
}

/* ESP_MSG_Timeout01_Callback function */
__weak void ESP_MSG_Timeout01_Callback(void *argument)
{
  /* USER CODE BEGIN ESP_MSG_Timeout01_Callback */
  
  /* USER CODE END ESP_MSG_Timeout01_Callback */
}

/* ESP_MSG_Timeout02_Callback function */
__weak void ESP_MSG_Timeout02_Callback(void *argument)
{
  /* USER CODE BEGIN ESP_MSG_Timeout02_Callback */
  
  /* USER CODE END ESP_MSG_Timeout02_Callback */
}

/* ESP_MSG_Timeout03_Callback function */
__weak void ESP_MSG_Timeout03_Callback(void *argument)
{
  /* USER CODE BEGIN ESP_MSG_Timeout03_Callback */
  
  /* USER CODE END ESP_MSG_Timeout03_Callback */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
     
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
