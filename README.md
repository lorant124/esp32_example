# ESP lib demo
## IDE settings
-Generated project add import as existing project to Atollic True studio.
-Add Esp lib project from git to generated project. In this case added as submodule with git to folder **../esp32_example/Components/esp32_lib**

After added library to project add in atollic project settings library path.
```
	#project->properties->c/c++ General->Paths and Symbols->Include->Add..->from workspace->Components/esp32_lib/inc*
	#project->properties->c/c++ General->Paths and Symbols->Source Location->Add Folder->Components/esp32_lib/src*
```	
## Main.c
In main.c init uarts.
```C
    #include "../Components/esp32_lib/inc/esp32.h"
    #include "../Components/esp32_lib/inc/debug_io.h"
	/*before freertos scheduler init call esp, debug uart setup*/
	esp32_uart_setup();
    debug_uart_setup();
```
## Freertos.c
In freertos.c include debug_io, esp32.h, esp32_thread.h.
```C
#include "queue.h"
#include "../Components/esp32_lib/Inc/debug_io.h"
#include "../Components/esp32_lib/Inc/esp32.h"
#include "../Components/esp32_lib/Inc/esp32_threads.h"

/*Default task demonstration in freertos.c:*/
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  serv_clt_msg_t msg = {"hello worldhello world\r\n" ,24 ,TCP_CLIENT_LINK_ID};
  /* Infinite loop */
  for(;;)
  {
	  HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_0);
	  if(esp.tcp_conn == TCP_CONNECTED)
	  {
		 xQueueSend(transparent_QueueHandle, &msg, 0);
	  }
    osDelay(500);
  }
  /* USER CODE END StartDefaultTask */
}
```

## Stm32f7xx_it.c 
  
In stm32 it .c file include cmsis_os and define as extern esp32_UART_Handle() function.  
Define as extern debug_msg_sync_semHandle semaphore.  
Add code to esp Tx UART DMA IRQ, ESP UART IRQ, debug Tx UART DMA IRQ  
		
```C  
	#include "cmsis_os.h"
	extern void esp32_UART_Handler(void); 
	extern osSemaphoreId_t debug_msg_sync_semHandle;

	/*Debug Uart DMA IRQ handler */
	DMA1_Stream4_IRQHandler()
	{
    	LL_DMA_ClearFlag_TC4(DMA1);
    	LL_DMA_DisableStream(DMA1, LL_DMA_STREAM_4);
    	osSemaphoreRelease(debug_msg_sync_semHandle);
	}
    /*Esp TX Uart DMA IRQ handler */
    void DMA1_Stream6_IRQHandler(void)
	{
    	LL_DMA_ClearFlag_TC6(DMA1);
    	LL_DMA_DisableStream(DMA1, LL_DMA_STREAM_6);
	}
    /*Esp Uart IRQ handler */
    void USART2_IRQHandler(void)
	{
    	esp32_UART_Handler();
	}
``` 